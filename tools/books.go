package tools

import (
	"book-product/model"
	"time"
)

func AddCategoryForBooks(book model.Books, value string) interface{} {
	type BookWithCategory struct {
		ID           uint64          `json:"id"`
		CreatedAt    time.Time       `json:"createdAt"`
		UpdatedAt    time.Time       `json:"updatedAt"`
		CreatedBy    string          `json:"createdBy"`
		UpdatedBy    string          `json:"updatedBy"`
		Image        string          `json:"image"`
		Name         string          `json:"name"`
		Author       string          `json:"author"`
		Details      string          `json:"details"`
		Price        float32         `json:"price"`
		Discount     uint8           `json:"discount"`
		CategoryId   uint64          `json:"categoryId"`
		CategoryName string          `json:"categoryName"`
		Reviews      []model.Reviews `json:"reviews"`
	}
	newBook := BookWithCategory{
		ID:           book.ID,
		CreatedAt:    book.CreatedAt,
		UpdatedAt:    book.UpdatedAt,
		CreatedBy:    book.CreatedBy,
		UpdatedBy:    book.UpdatedBy,
		Image:        book.Image,
		Name:         book.Name,
		Author:       book.Author,
		Details:      book.Details,
		Price:        book.Price,
		Discount:     book.Discount,
		CategoryId:   book.CategoryId,
		Reviews:      book.Reviews,
		CategoryName: value,
	}
	return newBook
}

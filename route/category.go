package route

import (
	"book-product/handlers"

	"github.com/gofiber/fiber/v2"
)

func SetupCategoryRoutes(app *fiber.App) {
	app.Post("/product/category", handlers.AddCategory)
}

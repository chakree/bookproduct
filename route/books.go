package route

import (
	"book-product/handlers"

	"github.com/gofiber/fiber/v2"
)

func SetupBookRoutes(app *fiber.App) {
	app.Get("/product/books", handlers.Decrypt, handlers.GetBooks)
	app.Get("/product/book/:id", handlers.Decrypt, handlers.GetBook)
	app.Post("/product/book", handlers.Decrypt, handlers.AddBook)
	app.Patch("/product/book/:id", handlers.Decrypt, handlers.EditBook)
	app.Delete("/product/book/:id", handlers.Decrypt, handlers.DeleteBook)
}

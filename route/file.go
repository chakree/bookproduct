package route

import (
	"book-product/handlers"

	"github.com/gofiber/fiber/v2"
)

func SetupFileRoutes(app *fiber.App) {
	app.Get("/product/file/:path", handlers.GetFile)
}

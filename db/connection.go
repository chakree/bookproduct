package db

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DBInstance struct {
	DB *gorm.DB
}

var DB DBInstance

func ConnectDB() (*gorm.DB, error) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
	dbconfig := os.Getenv("DB")
	db, err := gorm.Open(postgres.Open(dbconfig), &gorm.Config{})

	DB = DBInstance{
		DB: db,
	}
	return db, err
}

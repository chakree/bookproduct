package main

import (
	"book-product/db"
	"book-product/model"
	"book-product/route"
	"log"

	_ "book-product/docs"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/swagger"
)

// @title Fiber Example API
// @version 1.0
// @description This is a book api
// @host localhost:3000
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
// @BasePath /
func main() {
	db, err := db.ConnectDB()

	if err != nil {
		log.Fatal(err)
	}

	db.AutoMigrate(&model.Category{})
	db.AutoMigrate(&model.Books{})
	db.AutoMigrate(&model.Reviews{})

	app := fiber.New()
	app.Get("/swagger/*", swagger.HandlerDefault)
	app.Static("/", "./public")

	route.SetupFileRoutes(app)
	route.SetupCategoryRoutes(app)
	route.SetupBookRoutes(app)

	app.Listen(":3000")
}

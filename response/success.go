package response

type PaginationMessage struct {
	Count int64 `json:"count"`
	Data  any   `json:"data"`
}

type SuccessMessage struct {
	Message string `json:"message"`
}

package handlers

import (
	"book-product/db"
	"book-product/model"
	"book-product/response"
	"book-product/tools"
	"os"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
)

// @Security BearerAuth
// @Tags Books
// @Summary Get books
// @Description Get a books
// @ID get-books
// @Produce json
// @Param page query int false "page" Format(int64)
// @Param limit query int false "limit" Format(int64)
// @Param search query string false "search" Format(string)
// @Param startDate query string false "start date" Format(date)
// @Param endDate query string false "end date" Format(date)
// @Param sortBy query string false "sort by" Format(string)
// @Param sortDirection query string false "sort direction" Enum(ASC, DESC, null) default(null)
// @Router /product/books [get]
func GetBooks(c *fiber.Ctx) error {
	r := db.DB
	limit, _ := strconv.Atoi(c.Query("limit", "10"))
	page, _ := strconv.Atoi(c.Query("page", "1"))
	search := c.Query("search", "")
	startDateStr := c.Query("startDate", "")
	endDateStr := c.Query("endDate", "")
	sortBy := c.Query("sortBy", "updated_at")
	sortDirection := c.Query("sortDirection", "DESC")
	offset := (page - 1) * limit
	var book []model.Books
	var count int64
	defaultStartDate := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	defaultEndDate := time.Now()
	startDate, errTime := time.Parse("2006-01-02T15:04:05.000", startDateStr)
	if errTime != nil {
		startDate = defaultStartDate
	}
	endDate, errTime := time.Parse("2006-01-02T15:04:05.000", endDateStr)
	if errTime != nil {
		endDate = defaultEndDate
	}
	err := r.DB.Model(&model.Books{}).
		Offset(offset).
		Limit(limit).
		Where("name LIKE ?", "%"+search+"%").
		Where("updated_at BETWEEN ? AND ?", startDate, endDate).
		Find(&book).
		Order(sortBy + " " + sortDirection).
		Count(&count).Error

	if err != nil {
		return c.Status(500).JSON(err)
	}
	var bookWithCategory []any

	for i, queryCategory := range book {
		var category = model.Category{}
		r.DB.Model(&model.Category{}).First(&category, book[i].CategoryId)
		res := tools.AddCategoryForBooks(queryCategory, category.CategoryName)
		bookWithCategory = append(bookWithCategory, res)
	}

	result := response.PaginationMessage{
		Count: count,
		Data:  bookWithCategory,
	}

	return c.Status(200).JSON(result)
}

// @Security BearerAuth
// @Tags Books
// @Summary Get book by ID
// @Description Get a book by ID
// @ID get-book-by-id
// @Produce json
// @Param id path int true "book ID"
// @Param Authorization header string true "Bearer Token" default("Bearer YOUR_TOKEN_HERE")
// @Router /product/book/{id} [get]
func GetBook(c *fiber.Ctx) error {
	r := db.DB
	id := c.Params("id", "")
	if id == "" {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "ข้อมูลไม่ครบถ้วน",
		})
	}
	book := model.Books{}
	category := model.Category{}
	err := r.DB.Model(model.Books{}).Where("id = ?", id).Find(&book).Error
	if book.ID == 0 {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "ไม่พบหมายเลขไอดีนี้",
		})
	}
	if err != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: err.Error(),
		})
	}

	r.DB.Model(&model.Category{}).First(&category, book.CategoryId)
	result := tools.AddCategoryForBooks(book, category.CategoryName)
	return c.Status(200).JSON(result)
}

// @Security BearerAuth
// @Tags Books
// @Summary Add book
// @Description Add book
// @ID add-book
// @Produce json
// @Param image formData file true "Upload book image"
// @Param name formData string true "Upload book name"
// @Param author formData string true "Upload book author"
// @Param details formData string true "Upload book details"
// @Param price formData string true "Upload book price"
// @Param discount formData string true "Upload book discount"
// @Param categoryId formData string true "Upload book categoryId"
// @Param createdBy formData string true "Upload book createdBy"
// @Param updatedBy formData string true "Upload book updatedBy"
// @Router /product/book/ [post]
func AddBook(c *fiber.Ctx) error {
	r := db.DB
	image, errimage := c.FormFile("image")
	name := c.FormValue("name", "")
	author := c.FormValue("author", "")
	details := c.FormValue("details", "")
	price := c.FormValue("price")
	discount := c.FormValue("discount")
	categoryId := c.FormValue("categoryId")
	createdBy := c.FormValue("createdBy")
	updatedBy := c.FormValue("updatedBy")

	if errimage != nil && name != "" && author != "" && details != "" && price != "" && discount != "" && categoryId != "" {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "ข้อมูลไม่ครบถ้วน",
		})
	}
	priceFloat, errPrice := strconv.ParseFloat(price, 32)
	discountInt, errDiscount := strconv.ParseUint(discount, 10, 0)
	categoryIdInt, errcategoryId := strconv.ParseUint(categoryId, 10, 0)

	if errPrice != nil && errDiscount != nil && errcategoryId != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "ข้อมูลไม่ครบถ้วน",
		})
	}
	now := time.Now().UnixNano()
	book := model.Books{
		Image:      "book-" + strconv.FormatInt(now, 10) + ".png",
		Name:       name,
		Author:     author,
		Details:    details,
		Price:      float32(priceFloat),
		Discount:   uint8(discountInt),
		CategoryId: categoryIdInt,
		CreatedBy:  createdBy,
		UpdatedBy:  updatedBy,
	}
	c.SaveFile(image, "public/books/book-"+strconv.FormatInt(now, 10)+".png")

	result := r.DB.Create(&book).Error

	if result != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: result.Error(),
		})
	}
	return c.Status(200).JSON(response.SuccessMessage{
		Message: "บันทึกสำเร็จแล้ว",
	})
}

// @Security BearerAuth
// @Tags Books
// @Summary Edit book
// @Description Edit book
// @ID edit-book
// @Produce json
// @Param id path int true "book ID"
// @Param image formData file true "Upload book image"
// @Param name formData string true "Upload book name"
// @Param author formData string true "Upload book author"
// @Param details formData string true "Upload book details"
// @Param price formData string true "Upload book price"
// @Param discount formData string true "Upload book discount"
// @Param categoryId formData string true "Upload book categoryId"
// @Param updatedBy formData string true "Upload book updatedBy"
// @Router /product/book/{id} [patch]
func EditBook(c *fiber.Ctx) error {
	r := db.DB
	id := c.Params("id", "")
	image, errimage := c.FormFile("image")
	name := c.FormValue("name", "")
	author := c.FormValue("author", "")
	details := c.FormValue("details", "")
	price := c.FormValue("price")
	discount := c.FormValue("discount")
	categoryId := c.FormValue("categoryId")
	updatedBy := c.FormValue("updatedBy")

	if errimage != nil && name != "" && author != "" && details != "" && price != "" && discount != "" && categoryId != "" {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "ข้อมูลไม่ครบถ้วน",
		})
	}
	priceFloat, errPrice := strconv.ParseFloat(price, 32)
	discountInt, errDiscount := strconv.ParseUint(discount, 10, 0)
	categoryIdInt, errcategoryId := strconv.ParseUint(categoryId, 10, 0)
	if errPrice != nil && errDiscount != nil && errcategoryId != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "ข้อมูลไม่ครบถ้วน",
		})
	}
	checkBook := model.Books{}
	errCheckBook := r.DB.Model(model.Books{}).Where("id = ?", id).Find(&checkBook).Error
	if checkBook.ID == 0 {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "ไม่พบหมายเลขไอดีนี้",
		})
	}
	if errCheckBook != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: errCheckBook.Error(),
		})
	}
	os.Remove("public/books/" + checkBook.Image)
	now := time.Now().UnixNano()
	book := model.Books{
		Image:      "book-" + strconv.FormatInt(now, 10) + ".png",
		Name:       name,
		Author:     author,
		Details:    details,
		Price:      float32(priceFloat),
		Discount:   uint8(discountInt),
		CategoryId: categoryIdInt,
		UpdatedBy:  updatedBy,
	}
	c.SaveFile(image, "public/books/book-"+strconv.FormatInt(now, 10)+".png")

	result := r.DB.Where("id = ?", id).Updates(&book).Error

	if result != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: result.Error(),
		})
	}
	return c.Status(200).JSON(response.SuccessMessage{
		Message: "แก้ไขสำเร็จแล้ว",
	})
}

// @Security BearerAuth
// @Tags Books
// @Summary Delete book
// @Description Delete book
// @ID delete-book
// @Produce json
// @Param id path int true "book ID"
// @Router /product/book/{id} [delete]
func DeleteBook(c *fiber.Ctx) error {
	r := db.DB
	id := c.Params("id", "")
	book := model.Books{}

	errCheck := r.DB.Model(model.Books{}).Where("id = ?", id).Find(&book).Error

	if errCheck != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: errCheck.Error(),
		})
	}

	os.Remove("public/books/" + book.Image)

	result := r.DB.Where("id = ?", id).Delete(&book).Error

	if result != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: result.Error(),
		})
	}
	return c.Status(200).JSON(response.SuccessMessage{
		Message: "ลบสำเร็จแล้ว",
	})
}

package handlers

import (
	"book-product/db"
	"book-product/model"
	"book-product/response"

	"github.com/gofiber/fiber/v2"
)

func AddCategory(c *fiber.Ctx) error {
	r := db.DB
	category := model.Category{}

	err := c.BodyParser(&category)

	if err != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: err.Error(),
		})
	}

	result := r.DB.Create(&category).Error

	if result != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: result.Error(),
		})
	}

	return c.Status(200).JSON(&category)
}

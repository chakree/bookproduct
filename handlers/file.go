package handlers

import (
	"book-product/response"
	"io"
	"os"

	"github.com/gofiber/fiber/v2"
)

func GetFile(c *fiber.Ctx) error {
	path := c.Params("path", "")

	if path == "" {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: "Not Found Path",
		})
	}

	file, err := os.Open("public/books/" + path)

	if err != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: err.Error(),
		})
	}

	defer file.Close()

	c.Set("Content-Type", "image/jpeg")

	_, err = io.Copy(c.Response().BodyWriter(), file)
	if err != nil {
		return c.Status(400).JSON(response.ErrorMessage{
			Error: err.Error(),
		})
	}

	return nil
}

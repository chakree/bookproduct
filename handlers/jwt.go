package handlers

import (
	"book-product/response"
	"book-product/security"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func Decrypt(context *fiber.Ctx) error {
	header := context.GetReqHeaders()
	arrHeader := len(header["Authorization"])
	if arrHeader == 0 {
		return context.Status(401).JSON(response.ErrorMessage{
			Error: "Unauthorized",
		})
	}
	token := strings.Split(header["Authorization"][0], " ")[1]
	check := security.ParseToken(token)
	if !check {
		return context.Status(401).JSON(response.ErrorMessage{
			Error: "Unauthorized",
		})
	}
	return context.Next()
}

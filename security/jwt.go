package security

import (
	"book-product/model"
	"fmt"
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

func GenerateToken(email string) (string, error) {
	secretKey := []byte(os.Getenv("SECERT_KEY"))
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &model.Claims{
		Email: email,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
		},
	})

	tokenString, err := token.SignedString(secretKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func ParseToken(tokenString string) bool {
	secretKey := []byte(os.Getenv("SECERT_KEY"))
	token, err := jwt.ParseWithClaims(tokenString, &model.Claims{}, func(token *jwt.Token) (interface{}, error) {
		return secretKey, nil
	})

	if err != nil {
		return false
	}

	if claims, ok := token.Claims.(*model.Claims); ok {
		fmt.Println("Email => " + claims.Email)
		if claims.Email == "" {
			return false
		} else {
			return true
		}
	}

	return false
}

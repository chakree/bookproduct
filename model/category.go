package model

import "time"

type Category struct {
	ID           uint64    `gorm:"primaryKey"json:"id"`
	CreatedAt    time.Time `json:"createdAt"`
	UpdatedAt    time.Time `json:"updatedAt"`
	CategoryName string    `json:"categoryName"`
	CreatedBy    string    `json:"createdBy"`
	UpdatedBy    string    `json:"updatedBy"`
	Books        []Books
}

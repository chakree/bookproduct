package model

import "time"

type Reviews struct {
	ID        uint64    `gorm:"primaryKey"json:"id"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	UserId    string    `json:"userId"`
	BooksId   uint64    `json:"booksId"`
	Review    string    `json:"review"`
}

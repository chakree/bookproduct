package model

import "time"

type Books struct {
	ID         uint64    `gorm:"primaryKey"json:"id"`
	CreatedAt  time.Time `json:"createdAt"`
	UpdatedAt  time.Time `json:"updatedAt"`
	CreatedBy  string    `json:"createdBy"`
	UpdatedBy  string    `json:"updatedBy"`
	Image      string    `json:"image"`
	Name       string    `json:"name"`
	Author     string    `json:"author"`
	Details    string    `json:"details"`
	Price      float32   `json:"price"`
	Discount   uint8     `json:"discount"`
	CategoryId uint64
	Reviews    []Reviews
}
